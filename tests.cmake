include(CTest)

add_test(NAME check_hello_static_exit_zero
         COMMAND ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/hello-bin-static
        )

add_test(NAME check_hello_dynamic_exit_zero
         COMMAND ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/hello-bin-dynamic
        )
