cmake_minimum_required(VERSION 3.16)
project(hello-bin CXX)

# rules for static lib
add_executable(${HELLO_BIN_STATIC_NAME} src/main.cxx)
target_compile_features(${HELLO_BIN_STATIC_NAME} PRIVATE cxx_std_17)
target_link_libraries(${HELLO_BIN_STATIC_NAME} LINK_PUBLIC hello-lib-static)
target_link_options(${HELLO_BIN_STATIC_NAME} PRIVATE -static)

# rules for dynamic lib
add_executable(${HELLO_BIN_DYNAMIC_NAME} src/main.cxx)
target_compile_features(${HELLO_BIN_DYNAMIC_NAME} PRIVATE cxx_std_17)
target_link_libraries(${HELLO_BIN_DYNAMIC_NAME} LINK_PUBLIC hello-lib-dynamic)