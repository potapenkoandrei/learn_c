#include <iosfwd>
#include <memory>
#include <string>
#include <string_view>

#ifndef POTAP_DECLSPEC
#define POTAP_DECLSPEC
#endif

namespace potap
{
/// dendy gamepad emulation events
enum class event
{
    /// input events
    left_a_pressed,
    left_a_released,
    right_d_pressed,
    right_d_released,
    up_w_pressed,
    up_w_released,
    down_s_pressed,
    down_s_released,
    left_ctrl_pressed,
    left_ctrl_released,
    space_pressed,
    space_released,
    escape_pressed,
    escape_released,
    enter_pressed,
    enter_released,
    right_arrow_pressed,
    right_arrow_released,
    left_arrow_pressed,
    left_arrow_released,
    up_arrow_pressed,
    up_arrow_released,
    down_arrow_pressed,
    down_arrow_released,
    /// virtual console events
    turn_off
};

POTAP_DECLSPEC std::ostream& operator<<(std::ostream& stream, const event e);

class engine;

/// return not null on success
POTAP_DECLSPEC engine* create_engine();
POTAP_DECLSPEC void    destroy_engine(engine* e);

class POTAP_DECLSPEC engine
{
public:
    virtual ~engine();
    /// create main window
    /// on success return empty string
    virtual std::string initialize(std::string_view config) = 0;
    /// pool event from input queue
    /// return true if more events in queue
    virtual bool read_input(event& e) = 0;
    virtual void uninitialize()       = 0;
};

struct POTAP_DECLSPEC game
{
    virtual ~game()              = default;
    virtual void initialize()    = 0;
    virtual void on_event(event) = 0;
    virtual void update()        = 0;
    virtual void render() const  = 0;
};

} // end namespace potap

/// You have to implement next functions in your code for engine
/// to be able to load your library
extern "C" POTAP_DECLSPEC potap::game* create_game(potap::engine*);
extern "C" POTAP_DECLSPEC void      destroy_game(potap::game*);
