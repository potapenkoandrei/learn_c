#include <algorithm>
#include <array>
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string_view>
#include <thread>

#include "engine.hxx"

class potap_console_rotating_slash : public potap::game // inherite structure
{
public:
    explicit potap_console_rotating_slash(potap::engine&)
        : rotation_index{ 0 }
        //, rotations_chars{ { '-', '/', '|', '\\' } }
        , rotations_chars{ { 'A', 'n', 'd', 'r', 'e', 'i' } }
    {
    }
    void initialize() override {} //override - need to be override
    void on_event(potap::event) override {}
    void update() override
    {
        //using namespace std;
        ++rotation_index;
        rotation_index %= rotations_chars.size();
        using namespace std::chrono;
        std::this_thread::sleep_for(milliseconds(350));
    }
    void render() const override
    {
        const char current_symbol = rotations_chars.at(rotation_index);
        std::cout << "\b" << current_symbol << std::flush; // refresh cout now
        //constexpr bool more = false; // read about constexpr
        //if constexpr (more)
        //{
        //    std::cout << "\b\b\b" << current_symbol << current_symbol
        //              << current_symbol << std::flush;
        //}
    }

private:
    uint32_t                  rotation_index = 0;
    const std::array<char, 6> rotations_chars;
};

/// We have to export next two functions
POTAP_DECLSPEC potap::game* create_game(potap::engine* engine) // указатель на object om::engine with name "engine" 
{
    if (engine != nullptr)
    {
        return new potap_console_rotating_slash(*engine); //create the new object
    }
    return nullptr;
}

POTAP_DECLSPEC void destroy_game(potap::game* game)
{
    delete game;
}
