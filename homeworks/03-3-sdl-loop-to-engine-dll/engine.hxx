#include <iosfwd>
#include <string>
#include <string_view>

#ifndef potap_DECLSPEC
#define potap_DECLSPEC
#endif

namespace potap
{
/// dendy gamepad emulation events
enum class event
{
    /// input events
    left_a_pressed,
    left_a_released,
    right_d_pressed,
    right_d_released,
    up_w_pressed,
    up_w_released,
    down_s_pressed,
    down_s_released,
    left_ctrl_pressed,
    left_ctrl_released,
    space_pressed,
    space_released,
    escape_pressed,
    escape_released,
    enter_pressed,
    enter_released,
    left_arrow_pressed,
    left_arrow_released,
    right_arrow_pressed,
    right_arrow_released,
    up_arrow_pressed,
    up_arrow_released,
    down_arrow_pressed,
    down_arrow_released,
    right_ctrl_pressed,
    right_ctrl_released,
    /// virtual console events
    turn_off
};

potap_DECLSPEC std::ostream& operator<<(std::ostream& stream, const event e);

class engine;

/// return not null on success
potap_DECLSPEC engine* create_engine();
potap_DECLSPEC void destroy_engine(engine* e);

class potap_DECLSPEC engine
{
public:
    virtual ~engine();
    /// create main window
    /// on success return empty string
    virtual std::string initialize(std::string_view config) = 0;
    /// pool event from input queue
    /// return true if more events in queue
    virtual bool read_input(event& e) = 0;
    virtual void uninitialize()       = 0;
};

} // end namespace potap
