#include "engine.hxx"
//#include "SDL_render.h"
#include <algorithm>
#include <array>
#include <exception>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

#include <SDL.h>

namespace potap
{

static std::array<std::string_view, 25> event_names = {
    { /// input events
    "left_a_pressed",
    "left_a_released",
    "right_d_pressed",
    "right_d_released",
    "up_w_pressed",
    "up_w_released",
    "down_s_pressed",
    "down_s_released",
    "left_ctrl_pressed",
    "left_ctrl_released",
    "space_pressed",
    "space_released",
    "escape_pressed",
    "escape_released",
    "enter_pressed",
    "enter_released",
    "right_arrow_pressed",
    "right_arrow_released",
    "left_arrow_pressed",
    "left_arrow_released",
    "up_arrow_pressed",
    "up_arrow_released",
    "down_arrow_pressed",
    "down_arrow_released",
    //"mouse_pressed",
    //"mouse_released",
      // virtual console events
      "turn_off" }
};

std::ostream& operator<<(std::ostream& stream, const event e)
{
    std::uint32_t value   = static_cast<std::uint32_t>(e);
    std::uint32_t minimal = static_cast<std::uint32_t>(event::left_a_pressed);
    std::uint32_t maximal = static_cast<std::uint32_t>(event::turn_off);
    if (value >= minimal && value <= maximal)
    {
        stream << event_names[value];
        return stream;
    }
    else
    {
        throw std::runtime_error("too big event value");
    }
}

static std::ostream& operator<<(std::ostream& out, const SDL_version& v)
{
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}

struct bind
{
    bind(SDL_Keycode k, std::string_view s, event pressed, event released/*, keys potap_k*/)
    
        :   key(k)
        ,   name(s)
        ,   event_pressed(pressed)
        ,   event_released(released)
        //,   potap_key(potap_k)
       
    {    
    }

    SDL_Keycode      key;
    std::string_view name;
    event            event_pressed;
    event            event_released;
    //potap::keys      potap_key;   
};

const std::array<bind, 12> keys{
    { { SDLK_w,         "up_w",         event::up_w_pressed,        event::up_w_released },
      { SDLK_a,         "left_a",       event::left_a_pressed,      event::left_a_released },
      { SDLK_s,         "down_s",       event::down_s_pressed,      event::down_s_released },
      { SDLK_d,         "right_d",      event::right_d_pressed,     event::right_d_released },
      { SDLK_LCTRL,     "left_ctrl",    event::left_ctrl_pressed,   event::left_ctrl_released },
      { SDLK_SPACE,     "space",        event::space_pressed,       event::space_released },
      { SDLK_ESCAPE,    "escape",       event::escape_pressed,      event::escape_released },
      { SDLK_RETURN,    "enter",        event::enter_pressed,       event::enter_released }, 
      { SDLK_RIGHT,     "right_arrow",  event::right_arrow_pressed, event::right_arrow_released },
      { SDLK_LEFT,      "left_arrow",   event::left_arrow_pressed,  event::left_arrow_released },
      { SDLK_UP,        "up_arrow",     event::up_arrow_pressed,    event::up_arrow_released },
      { SDLK_DOWN,      "down_arrow",   event::down_arrow_pressed,  event::down_arrow_released }
      }
};

static bool check_input(const SDL_Event& e, const bind*& result)
{
    using namespace std;

    const auto it = find_if(begin(keys), end(keys), [&](const bind& b) {
        return b.key == e.key.keysym.sym;
    });

    if (it != end(keys))
    {
        result = &(*it);
        return true;
    }
    return false;
}

class engine_impl final : public engine
{
public:
    /// create main window
    /// on success return empty string
    std::string initialize(std::string_view /*config*/) final
    {
        using namespace std;

        stringstream serr;

        SDL_version compiled = { 0, 0, 0 };
        SDL_version linked   = { 0, 0, 0 };

        SDL_VERSION(&compiled)
        SDL_GetVersion(&linked);

        if (SDL_COMPILEDVERSION !=
            SDL_VERSIONNUM(linked.major, linked.minor, linked.patch))
        {
            serr << "warning: SDL2 compiled and linked version mismatch: "
                 << compiled << " " << linked << endl;
        }

        const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
        if (init_result != 0)
        {
            const char* err_message = SDL_GetError();
            serr << "error: failed call SDL_Init: " << err_message << endl;
            return serr.str();
        }
       
        SDL_Window* const window = SDL_CreateWindow(
            "The RED window =)", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 400,
            ::SDL_WINDOW_OPENGL);

        SDL_Renderer *ren = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

        SDL_Surface *screenSurface = SDL_LoadBMP("ussr.bmp"); ///home/andrei/learn/git/my/learn_c/homeworks/03-3-sdl-loop-to-engine-dll/

        SDL_Texture *tex = SDL_CreateTextureFromSurface(ren, screenSurface);

        SDL_RenderClear(ren);
        SDL_RenderCopy(ren, tex, NULL, NULL);
        SDL_RenderPresent(ren);

        if (window == nullptr) 
        {
            const char* err_message = SDL_GetError();
            serr << "151 error: failed call SDL_CreateWindow : " << err_message
                 << endl;
            SDL_Quit();
            return serr.str();
        }

        if (screenSurface == nullptr)
        {
	        const char* err_message = SDL_GetError();
            serr << "160 error: failed call SDL_LoadBMP: " << err_message
                 << endl;
            SDL_Quit();
            return serr.str();
        }

        if (ren == nullptr)
        {
	        const char* err_message = SDL_GetError();
            serr << "169 error: failed call Render: " << err_message
                 << endl;
            SDL_Quit();
            return serr.str();
        }

        if (tex == nullptr)
        {
	        const char* err_message = SDL_GetError();
            serr << "178 error: failed call Texture: " << err_message
                 << endl;
            SDL_Quit();
            return serr.str();
        }     

        /* Open the first available controller.                     try add second controller!!!!!!!   */
        SDL_GameController* controller = NULL;
        for (int i = 0; i < SDL_NumJoysticks(); ++i)
        {
            if (SDL_IsGameController(i))
            {
                controller = SDL_GameControllerOpen(i);
                if (controller)
                {
                    break;
                }
                else
                {
                    fprintf(stderr, "Could not open gamecontroller %i: %s\n", i,
                            SDL_GetError());
                }
            }
        }

    return "";
    }
    /// pool event from input queue
    /// return true if more events in queue
    bool read_input(event& e) final
    {
        using namespace std;
        // collect all events from SDL
        SDL_Event sdl_event;
        if (SDL_PollEvent(&sdl_event))
        {
            const bind* binding = nullptr;

            if (sdl_event.type == SDL_QUIT)
            {
                e = event::turn_off;
                return true;
            }
            else if (sdl_event.type == SDL_KEYDOWN)
            {
                if (check_input(sdl_event, binding))
                {
                    e = binding->event_pressed;
                    return true;
                }
            }
            else if (sdl_event.type == SDL_KEYUP)
            {
                if (check_input(sdl_event, binding))
                {
                    e = binding->event_released;
                    return true;
                }
            }
            else if (sdl_event.type == SDL_CONTROLLERDEVICEADDED)
            {
                // TODO map controller to user
                std::cerr << "controller added" << std::endl;
                // continue with next event in queue
                return read_input(e);
            }
            else if (sdl_event.type == SDL_CONTROLLERDEVICEREMOVED)
            {
                std::cerr << "controller removed" << std::endl;
            }
            else if (sdl_event.type == SDL_CONTROLLERBUTTONDOWN ||
                     sdl_event.type == SDL_CONTROLLERBUTTONUP)
            {
                // TODO finish implementation
                if (sdl_event.cbutton.state == SDL_PRESSED)
                {
                    e = event::escape_pressed;
                }
                else
                {
                    e = event::escape_released;
                }
                return true;
            }
        }
        return false;
    }

void uninitialize() final {}
};

static bool already_exist = false;

engine* create_engine()
{
    if (already_exist)
    {
        throw std::runtime_error("engine already exist");
    }
    engine* result = new engine_impl();
    already_exist  = true;
    return result;
}

void destroy_engine(engine* e)
{
    if (already_exist == false)
    {
        throw std::runtime_error("engine not created");
    }
    if (nullptr == e)
    {
        throw std::runtime_error("e is nullptr");
    }
    delete e;
}

engine::~engine() {}

} // end namespace potap
