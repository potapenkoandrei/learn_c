#pragma once

#include <vector>

[[maybe_unused]] static double GRAVITY_CONST = 6.67;
[[maybe_unused]] static double GRAVITY_EARTH = 9.81;

class coordinates {
public:
    coordinates() = default;
    coordinates(const double& x_, const double& y_)
        : x {x_}, y{y_}
    {}
    double x;
    double y;

    friend bool operator==(const coordinates& lhs, const coordinates& rhs){ return (lhs.x == rhs.x) && (lhs.y == rhs.y);}
};

class gravity_point{
public:
    gravity_point() = default;
    gravity_point(const double& x_, const double& y_, const double& m_)
        : coord{x_, y_}
        , mass{m_}
    {}
    coordinates coord;
    double mass;
};

class force{
public:
    force() = default;
    force(const coordinates& start_, const double& f_, const double& a_)
        : dot{start_}
        , f{f_}
        , angle{a_}
    {}
    coordinates dot;
    double f;
    double angle;
};

class physics_object {
public:
    physics_object()=default;
    ~physics_object()=default;

    int add_new_gravity_point(const gravity_point& p);
    int remove_gravity_point(const gravity_point& p);
    int add_new_force(const force& f_);
    int remove_force(const force& f_);
    double calculate_distance(const gravity_point& gp);
    force gravity_force_calculating(const gravity_point& gp);
    int update();

    // removed unused constructors
    physics_object(const physics_object&) = delete;
    const physics_object& operator=(const physics_object&) = delete;
    physics_object(physics_object&&) = delete;
    physics_object& operator=(physics_object&&) = delete;

private:
    coordinates coord;
    float mass;
    force f;
    std::vector<gravity_point> points;
    std::vector<force> forces;
};
