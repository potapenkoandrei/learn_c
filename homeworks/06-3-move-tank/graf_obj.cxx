#include "graf_obj.hxx"

class graf_obj
{
public:
//    om::engine* engine;
//    om::texture* tex;
//    om::vertex_buffer* buf;
//    std::string path_to_file;
//    std::array<om::tri2, 2> tr;
    bool set_texture (const std::string& t) 
    {
         tex = engine->create_texture(t);
            if (nullptr == tex)
            {
                std::cerr << "failed load texture " << t << std::endl;
                return EXIT_FAILURE;
            }
        return EXIT_SUCCESS;
    }

    bool set_vertex_buffer (const std::string& path) 
    {
        std::ifstream file(path);
        if (!file)
            {
                std::cerr << "can't load vert_tex_buffer" << path << std::endl;
                return EXIT_FAILURE;
            }
            else
            {

                file >> tr[0] >> tr[1];
                buf = engine->create_vertex_buffer(&tr[0], tr.size());
                if (buf == nullptr)
                {
                    std::cerr << "can't create vertex buffer" << std::endl;
                    return EXIT_FAILURE;
                }
            }
        return EXIT_SUCCESS;
    }

    bool init (const om::engine* e,const std::string& f1, const std::string& f2)
    {   
        engine = e;
        set_texture (f1);
        set_vertex_buffer(f2);
        return EXIT_SUCCESS;
    } 
};
