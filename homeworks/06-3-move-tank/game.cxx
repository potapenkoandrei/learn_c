#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <chrono>
#include <ctime>
//#include <numbers>
#include <string_view>

#include "engine.hxx"
//#include "ani2d.hxx"
//#include "sprite.hxx"

//#include "graf_obj.hxx"
//#include "physics_object.hxx"

#define SPEED_MAX 0.015f
#define SCALE_KOEF 1.333333f

int main(int /*argc*/, char* /*argv*/[])
{
    std::unique_ptr<om::engine, void (*)(om::engine*)> engine(
        om::create_engine(), om::destroy_engine);

    const std::string error = engine->initialize("");
    if (!error.empty())
    {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

////////////////////////////////////////////////////////////////////
    om::texture* texture_back = engine->create_texture("resourses/space.png");
    if (nullptr == texture_back)
    {
        std::cerr << "failed load texture\n";
        return EXIT_FAILURE;
    }

    om::vertex_buffer* vertex_buf_back = nullptr;

    std::ifstream file2("buffers/vert_tex_back.txt");
    if (!file2)
    {
        std::cerr << "can't load vert_tex_back.txt\n";
        return EXIT_FAILURE;
    }
    else
    {
        std::array<om::tri2, 2> tr;
        file2 >> tr[0] >> tr[1];
        vertex_buf_back = engine->create_vertex_buffer(&tr[0], tr.size());
        if (vertex_buf_back == nullptr)
        {
            std::cerr << "can't create vertex buffer\n";
            return EXIT_FAILURE;
        }
    }
//////////////////////////////////////////////////////////////////
    om::texture* texture_sun = engine->create_texture("resourses/sun.png");
    if (nullptr == texture_sun)
    {
        std::cerr << "failed load texture sun\n";
        return EXIT_FAILURE;
    }

    om::vertex_buffer* vertex_buf_sun = nullptr;

    std::ifstream file3("buffers/vert_tex_sun.txt");
    if (!file3)
    {
        std::cerr << "can't load vert_tex_sun.txt\n";
        return EXIT_FAILURE;
    }
    else
    {
        std::array<om::tri2, 2> tr;
        file3 >> tr[0] >> tr[1];
        vertex_buf_sun = engine->create_vertex_buffer(&tr[0], tr.size());
        if (vertex_buf_sun == nullptr)
        {
            std::cerr << "can't create vertex buffer sun\n";
            return EXIT_FAILURE;
        }
    }
    //////////////////////////////////////////////////////////////////
    om::texture* texture_moon = engine->create_texture("resourses/moon.png");
    if (nullptr == texture_moon)
    {
        std::cerr << "failed load texture moon\n";
        return EXIT_FAILURE;
    }

    om::vertex_buffer* vertex_buf_moon = nullptr;

    std::ifstream file4("buffers/vert_tex_moon.txt");
    if (!file4)
    {
        std::cerr << "can't load vert_tex_moon.txt\n";
        return EXIT_FAILURE;
    }
    else
    {
        std::array<om::tri2, 2> tr;
        file4 >> tr[0] >> tr[1];
        vertex_buf_moon = engine->create_vertex_buffer(&tr[0], tr.size());
        if (vertex_buf_moon == nullptr)
        {
            std::cerr << "can't create vertex buffer moon\n";
            return EXIT_FAILURE;
        }
    }

    //////////////////////////////////////////////////////////////////
    om::texture* texture_neptune = engine->create_texture("resourses/neptune.png");
    if (nullptr == texture_neptune)
    {
        std::cerr << "failed load texture neptune\n";
        return EXIT_FAILURE;
    }

    om::vertex_buffer* vertex_buf_neptune = nullptr;

    std::ifstream file5("buffers/vert_tex_neptune.txt");
    if (!file5)
    {
        std::cerr << "can't load vert_tex_neptune.txt\n";
        return EXIT_FAILURE;
    }
    else
    {
        std::array<om::tri2, 2> tr;
        file5 >> tr[0] >> tr[1];
        vertex_buf_neptune = engine->create_vertex_buffer(&tr[0], tr.size());
        if (vertex_buf_neptune == nullptr)
        {
            std::cerr << "can't create vertex buffer neptune\n";
            return EXIT_FAILURE;
        }
    }
////////////////////////////////////////////////////////////////////
    om::texture* texture_ship = engine->create_texture("resourses/ship_body.png");
    if (nullptr == texture_ship)
    {
        std::cerr << "failed load texture\n";
        return EXIT_FAILURE;
    }

    om::vertex_buffer* vertex_buf_ship = nullptr;

    std::ifstream file("buffers/vert_tex_color_ship.txt");
    if (!file)
    {
        std::cerr << "can't load vert_tex_color_ship.txt\n";
        return EXIT_FAILURE;
    }
    else
    {
        std::array<om::tri2, 2> tr;
        file >> tr[0] >> tr[1];
        vertex_buf_ship = engine->create_vertex_buffer(&tr[0], tr.size());
        if (vertex_buf_ship == nullptr)
        {
            std::cerr << "can't create vertex buffer ship\n";
            return EXIT_FAILURE;
        }
    }
    ////////////////////////////////////////////////////////////////////
    om::texture* texture_ship_fire = engine->create_texture("resourses/ship_fire.png");
    if (nullptr == texture_ship_fire)
    {
        std::cerr << "failed load texture\n";
        return EXIT_FAILURE;
    }

    om::vertex_buffer* vertex_buf_ship_fire = nullptr;

    std::ifstream file6("buffers/vert_tex_color_ship_fire.txt");
    if (!file5)
    {
        std::cerr << "can't load vert_tex_color_ship_fire.txt\n";
        return EXIT_FAILURE;
    }
    else
    {
        std::array<om::tri2, 2> tr;
        file6 >> tr[0] >> tr[1];
        vertex_buf_ship_fire = engine->create_vertex_buffer(&tr[0], tr.size());
        if (vertex_buf_ship_fire == nullptr)
        {
            std::cerr << "can't create vertex buffer ship fire\n";
            return EXIT_FAILURE;
        }
    }



/////////////////////////////////////////////////////////////////////////

    bool continue_loop = true;

    om::vec2    current_ship_pos(-0.8f, 0.8f);
    float       current_ship_direction(0.f);
    float       current_ship_scale(1.f);
    float       ship_speed_x_pos(0.f);
    float       ship_speed_x_neg(0.f);
    float       ship_speed_y_pos(0.f);
    float       ship_speed_y_neg(0.f);
    const float pi = 3.1415926f;//std::numbers::pi_v<float>;

    int start_delta_time = clock();

    while (continue_loop)
    {
        om::event event;

        while (engine->read_event(event))
        {
            std::cout << event << std::endl;
            switch (event)
            {
                case om::event::turn_off:
                    continue_loop = false;
                    break;

                default:
                    break;
            }
        }

        om::mat2x3 back;
        engine->render(*vertex_buf_back, texture_back, back);

        om::mat2x3 sun;
        om::vec2   sun_pos(-0.5f, -0.5f);
        om::mat2x3 move_s = om::mat2x3::move(sun_pos);
        om::mat2x3 aspect_s = om::mat2x3::scale(current_ship_scale, SCALE_KOEF);
        float time_s = engine->get_time_from_init();
        om::mat2x3 rot_s = om::mat2x3::rotation(-time_s);
        sun = rot_s * move_s * aspect_s;
        engine->render(*vertex_buf_sun, texture_sun, sun);

        om::mat2x3 neptune;
        om::vec2   neptune_pos(-0.1f, 0.5f);
        om::mat2x3 move_neptune = om::mat2x3::move(neptune_pos);
        om::mat2x3 aspect_neptune = om::mat2x3::scale(current_ship_scale, SCALE_KOEF);
        float time_neptune = engine->get_time_from_init();
        om::mat2x3 rot_neptune = om::mat2x3::rotation(-time_neptune);
        neptune = rot_neptune * move_neptune * aspect_neptune;
        engine->render(*vertex_buf_neptune, texture_neptune, neptune);

        om::mat2x3 moon;
        om::vec2   moon_pos(0.3f, -0.4f);
        om::mat2x3 move_m = om::mat2x3::move(moon_pos);
        om::mat2x3 aspect_m = om::mat2x3::scale(current_ship_scale, SCALE_KOEF);
        float time_m = engine->get_time_from_init();
        om::mat2x3 rot_m = om::mat2x3::rotation(time_m);
        moon = rot_m * move_m * aspect_m;
        engine->render(*vertex_buf_moon, texture_moon, moon);

        om::vec2   fire_pos(0.f, 0.f);

        int end_delta_time = clock();
        if ((end_delta_time - start_delta_time) > 333)
        {

            if (engine->is_key_down(om::keys::select))
            {
                continue_loop = false;
            }
            if (engine->is_key_down(om::keys::left))
            {
                if (current_ship_pos.x > -0.88f / current_ship_scale)
                {   ship_speed_x_neg += 0.0001f;
                    if (ship_speed_x_neg > SPEED_MAX) {ship_speed_x_neg = SPEED_MAX;}
                }
                    current_ship_direction = pi / 2.f;
                    fire_pos = om::vec2(current_ship_pos.x + 0.07f, current_ship_pos.y);
             } else {
                     ship_speed_x_neg -= 0.0001f;
                     if (ship_speed_x_neg <= 0.f) {ship_speed_x_neg = 0.f;}
             }

            if (engine->is_key_down(om::keys::right))
            {
                if (current_ship_pos.x < 0.88f / current_ship_scale)
                {   ship_speed_x_pos += 0.0001f;
                    if (ship_speed_x_pos > SPEED_MAX) {ship_speed_x_pos = SPEED_MAX;}
                }
                    current_ship_direction = -pi / 2.f;
                    fire_pos = om::vec2(current_ship_pos.x - 0.07f, current_ship_pos.y);
             } else {
                    ship_speed_x_pos -= 0.0001f;
                    if (ship_speed_x_pos <= 0.f) {ship_speed_x_pos = 0.f;}
             }

            if (engine->is_key_down(om::keys::up))
            {
                if (current_ship_pos.y < 0.65f / current_ship_scale)
                {   ship_speed_y_pos += 0.0001f;
                    if (ship_speed_y_pos > SPEED_MAX) {ship_speed_y_pos = SPEED_MAX;}
                }
                    current_ship_direction = -pi;
                    fire_pos = om::vec2(current_ship_pos.x, current_ship_pos.y - 0.07f);
             } else {
                    ship_speed_y_pos -= 0.0001f;
                    if (ship_speed_y_pos <= 0.f) {ship_speed_y_pos = 0.f;}
             }

            if (engine->is_key_down(om::keys::down))
            {
                if (current_ship_pos.y > -0.65f / current_ship_scale)
                {   ship_speed_y_neg += 0.0001f;
                    if (ship_speed_y_neg > SPEED_MAX) {ship_speed_y_neg = SPEED_MAX;}
                }
                    current_ship_direction = 0.f;
                    fire_pos = om::vec2(current_ship_pos.x, current_ship_pos.y + 0.07f);
             } else {
                    ship_speed_y_neg -= 0.0001f;
                    if (ship_speed_y_neg <= 0.f) {ship_speed_y_neg = 0.f;}
             }


            current_ship_pos.x = current_ship_pos.x + ship_speed_x_pos - ship_speed_x_neg;
            current_ship_pos.y = current_ship_pos.y + ship_speed_y_pos - ship_speed_y_neg;

            if (current_ship_pos.x > 0.88f) {current_ship_pos.x = 0.88f;}
            if (current_ship_pos.x < -0.88f) {current_ship_pos.x = -0.88f;}

            if (current_ship_pos.y > 0.65f) {current_ship_pos.y = 0.65f;}
            if (current_ship_pos.y < -0.65f) {current_ship_pos.y = -0.65f;}

            current_ship_pos = engine->new_coord(current_ship_pos, sun_pos, 1.2f);
            current_ship_pos = engine->new_coord(current_ship_pos, moon_pos, 1.0f);
            current_ship_pos = engine->new_coord(current_ship_pos, neptune_pos, 0.8f);

            if  ((engine->is_key_down(om::keys::up)) && (engine->is_key_down(om::keys::left)))
            {
                current_ship_direction = 0.75f * pi;
                fire_pos = om::vec2(current_ship_pos.x + 0.05f, current_ship_pos.y - 0.05f);
            }
            if  ((engine->is_key_down(om::keys::up)) && (engine->is_key_down(om::keys::right)))
            {
                current_ship_direction = 0.75f * -pi;
                fire_pos = om::vec2(current_ship_pos.x - 0.05f, current_ship_pos.y - 0.05f);
            }
            if  ((engine->is_key_down(om::keys::down)) && (engine->is_key_down(om::keys::left)))
            {
                current_ship_direction = 0.25f * pi;
                fire_pos = om::vec2(current_ship_pos.x + 0.05f, current_ship_pos.y + 0.05f);
            }
            if  ((engine->is_key_down(om::keys::down)) && (engine->is_key_down(om::keys::right)))
            {
                current_ship_direction = 0.25f * -pi;
                fire_pos = om::vec2(current_ship_pos.x - 0.05f, current_ship_pos.y + 0.05f);
            }

            start_delta_time = end_delta_time;
        }


        om::mat2x3 move   = om::mat2x3::move(current_ship_pos);
        om::mat2x3 aspect = om::mat2x3::scale(current_ship_scale, SCALE_KOEF + (current_ship_scale - 1) * SCALE_KOEF);
        om::mat2x3 rot    = om::mat2x3::rotation(current_ship_direction);
        om::mat2x3 ship      = rot * move * aspect;


        om::mat2x3 move_f   = om::mat2x3::move(fire_pos);
        om::mat2x3 aspect_f = om::mat2x3::scale(current_ship_scale, SCALE_KOEF + (current_ship_scale - 1) * SCALE_KOEF);
        if (fire_pos.x == 0.f && fire_pos.y == 0.f) {aspect_f = om::mat2x3::scale(current_ship_scale, 0.f);}
        om::mat2x3 rot_f    = om::mat2x3::rotation(current_ship_direction);
        om::mat2x3 ship_f      = rot_f * move_f * aspect_f;


        if ((sqrtf((current_ship_pos.x - sun_pos.x)*(current_ship_pos.x - sun_pos.x) +
                   (current_ship_pos.y - sun_pos.y)*(current_ship_pos.y - sun_pos.y)) < 0.12f) ||
                (sqrtf((current_ship_pos.x - moon_pos.x)*(current_ship_pos.x - moon_pos.x) +
                   (current_ship_pos.y - moon_pos.y)*(current_ship_pos.y - moon_pos.y)) < 0.1f) ||
            (sqrtf((current_ship_pos.x - neptune_pos.x)*(current_ship_pos.x - neptune_pos.x) +
                   (current_ship_pos.y - neptune_pos.y)*(current_ship_pos.y - neptune_pos.y)) < 0.08f))
        {
//            engine->render(*vertex_buf_ship, texture_ship, ship);// explosive
        } else {
            engine->render(*vertex_buf_ship, texture_ship, ship);
            engine->render(*vertex_buf_ship_fire, texture_ship_fire, ship_f);
        }

// add exploise ship when bit  planet

        engine->swap_buffers();

    }

    engine->uninitialize();

    return EXIT_SUCCESS;
}


