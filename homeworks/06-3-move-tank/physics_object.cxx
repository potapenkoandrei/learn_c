#include "./physics_object.hxx"

#include <algorithm>
#include <cmath>



int physics_object::add_new_gravity_point(const gravity_point& p)
{
    points.push_back(p);
    return 0;
}

int physics_object::remove_gravity_point(const gravity_point &p)
{
    const auto it = std::find_if(points.begin(), points.end(),
                           [&](const gravity_point& gp){return (gp.coord.x == p.coord.x)
                                                            && (gp.coord.y == p.coord.y)
                                                            && (gp.mass == p.mass);});

    if (it != points.end()){
        points.erase(it);
        return 0;
    } else {
        return 1;
    }
}

int physics_object::add_new_force(const force &f_)
{
    forces.push_back(f_);
    return 0;
}

int physics_object::remove_force(const force &f_)
{
    const auto it = std::find_if(forces.begin(), forces.end(),
                           [&](const force& tmp){return (tmp.angle == f_.angle)
                                                     && (tmp.f == f_.f)
                                                     && (tmp.dot == f_.dot);});

    if (it != forces.end()){
        forces.erase(it);
        return 0;
    } else {
        return 1;
    }
}

double physics_object::calculate_distance(const gravity_point &gp)
{
    return sqrt(pow(coord.x - gp.coord.x, 2) + pow(coord.y - gp.coord.y, 2));
}

force physics_object::gravity_force_calculating(const gravity_point& gp)
{
    force tmp;

    tmp.f = GRAVITY_CONST * gp.mass * mass / pow(calculate_distance(gp),2);
    tmp.angle = 0;
    tmp.dot.x = coord.x;
    tmp.dot.y = coord.y;

    return tmp;
}

int physics_object::update()
{
    forces.clear();

    for (auto x : points){
        forces.push_back(gravity_force_calculating(x));
    }

    for (auto x : forces){
        // calculating results force
        // return this->f
    }

    // update internal coordinates

    return 0;
}
