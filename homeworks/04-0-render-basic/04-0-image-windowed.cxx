#include "04_triangle_interpolated_render.hxx"

#include <SDL2/SDL.h>
#include <memory.h>
#include <cmath>
#include <cstdlib>
#include <time.h>
#include <algorithm>
#include <chrono>
#include <iostream>


int main(int, char**)
{
    using namespace std;

    if (0 != SDL_Init(SDL_INIT_EVERYTHING))
    {
        cerr << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }

    constexpr size_t width  = 640;
    constexpr size_t height = 480;

    SDL_Window* window = SDL_CreateWindow("runtime soft render",
                                          SDL_WINDOWPOS_CENTERED,
                                          SDL_WINDOWPOS_CENTERED,
                                          width,
                                          height,
                                          SDL_WINDOW_OPENGL);
    if (window == nullptr)
    {
        cerr << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }

    SDL_Renderer* renderer =
        SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == nullptr)
    {
        cerr << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }

    const color black = { 0, 0, 0 };

    canvas texture(0, 0);
    texture.load_image("leo.ppm");

    canvas image(width, height);

    triangle_interpolated interpolated_render(image, width, height);

    struct program : gfx_program
    {
        uniforms uniforms_;
        void     set_uniforms(const uniforms& a_uniforms) override
        {
            uniforms_ = a_uniforms;
        }
        vertex vertex_shader(const vertex& v_in) override
        {
            vertex out = v_in;

            return out;
        }
        color fragment_shader(const vertex& v_in) override
        {
            color out;

            float tex_x = v_in.f5; // 0..1
            float tex_y = v_in.f6; // 0..1

            canvas* texture = uniforms_.texture0;

            size_t tex_width  = texture->get_width();
            size_t tex_height = texture->get_height();

            size_t t_x = static_cast<size_t>((tex_width) * tex_x);
            size_t t_y = static_cast<size_t>((tex_height ) * tex_y);

            //size_t t_x = static_cast<size_t>((tex_width - 1) / width * tex_x);
            //size_t t_y = static_cast<size_t>((tex_height - 1) / height * tex_y);

            out = texture->get_pixel(t_x, t_y);

            return out;
        }
    } program01;

    struct grayscale : program
    {
        color fragment_shader(const vertex& v_in) override
        {
            color out;

            float tex_x = v_in.f5; // 0..1
            float tex_y = v_in.f6; // 0..1

            canvas* texture = uniforms_.texture0;

            size_t tex_width  = texture->get_width();
            size_t tex_height = texture->get_height();

            size_t t_x = static_cast<size_t>((tex_width) * tex_x);
            size_t t_y = static_cast<size_t>((tex_height) * tex_y);

            //size_t t_x = static_cast<size_t>((tex_width - 1) / width * tex_x);
            //size_t t_y = static_cast<size_t>((tex_height - 1) / height * tex_y);

            out = texture->get_pixel(t_x, t_y);

            uint8_t gray = static_cast<uint8_t>(
                0.2125 * out.r + 0.7152 * out.g + 0.0721 * out.b);

            out.r = gray;
            out.g = gray;
            out.b = gray;

            return out;
        }
    } program02;

    struct rotate_image : program
    {
        vertex vertex_shader(const vertex& v_in) override
        {
            vertex out = v_in;

            out.f0 -= (width / 2);
            out.f1 -= (height / 2);

            out.f0 *= 0.5;
            out.f1 *= 0.5;
            double x = out.f0; double y = out.f1;
            // rotate
            double alpha = (3.14159 / 4) * uniforms_.f7 * -1;
            //double x     = out.f5*746;//*texture.get_width ();
            //double y     = out.f6*1327;//*texture.get_height ();
            //out.f0       = x * std::cos(alpha) - y * std::sin(alpha);
            //out.f1       = x * std::sin(alpha) + y * std::cos(alpha);

            out.f0       = x * std::cos(alpha) - y * std::sin(alpha);
            out.f1       = x * std::sin(alpha) + y * std::cos(alpha);

            out.f0 += (width / 2);
            //if (out.f0 >= width){out.f0= width-1;}
            out.f1 += (height / 2);
            //if (out.f1 >= height){out.f1= height-1;}
            return out;
        }
    } program03;


    struct mirror_image_x : program
    {
        vertex vertex_shader(const vertex& v_in) override
        {
            vertex out;
            out = v_in;

            out.f0 = width - out.f0 - 1;

            return out;
        }
    } program04;

    struct big_mirror : program
    {
        double mouse_x{};
        double mouse_y{};
        double radius{};

        void set_uniforms(const uniforms& a_uniforms) override
        {
            mouse_x = a_uniforms.f3;
            mouse_y = a_uniforms.f4;
            radius  = a_uniforms.f5;
        }

        vertex vertex_shader(const vertex& v_in) override
        {
            vertex out = v_in;

            double x = out.f0;
            double y = out.f1;

            double dx = x - mouse_x;
            double dy = y - mouse_y;
            if (dx * dx + dy * dy < radius * radius)
            {
                // un magnet from mouse
                double len = std::sqrt(dx * dx + dy * dy);
                if (len > 0)
                {
                    // normalize vector from vertex to mouse pos
                    double norm_dx = dx / len;
                    double norm_dy = dy / len;
                    // find position of point on radius from mouse pos in center
                    double radius_pos_x = mouse_x + norm_dx * radius;
                    double radius_pos_y = mouse_y + norm_dy * radius;
                    // find middle point
                    x = (x + radius_pos_x) / 2;
                    y = (y + radius_pos_y) / 2;
                }
             }
            //assert(!std::isnan(x));
            //assert(!std::isnan(y));
            out.f0 = x;
            out.f1 = y;

            return out;
        }

    } program05;

    struct jumping : program
    {
        //double  rnd_jupm {};
        //int rnd_number{};

        vertex vertex_shader(const vertex& v_in) override
        {
            vertex out = v_in;


            /*double x = round(out.f0 / 10);
            double y = round(out.f1 / 10);

            rnd_number = rand() % 9;

            x = x * rnd_number;
            y = y * rnd_number;

            assert(!std::isnan(x));
            assert(!std::isnan(y));
            out.f0 = x;
            out.f1 = y;  */

            return out;
        }
    } program06;

    std::array<gfx_program*, 6> programs{ &program01,
                                          &program02,
                                          &program03,
                                          &program04,
                                          &program05,
                                          &program06
                                        };
    size_t current_program_index = 0;
    gfx_program* current_program = programs.at(current_program_index);

/*        //clang-format off
        //                                x  y                    r  g  b  tx ty

        std::vector<vertex> triangle_v{ { 0, 0,                  1, 1, 1, 0, 0, 0 },
                                        { width - 1, height - 1, 1, 1, 1, 1, 1, 0 },
                                        { 0, height - 1,         1, 1, 1, 0, 1, 0 },
                                        { width - 1, 0,          1, 1, 1, 1, 0, 0 } };

        // clang-format on
        std::vector<uint16_t> indexes_v{ { 0, 1, 2, 0, 3, 1 } };
*/

    std::vector<vertex> triangle_v;

    const size_t cell_x_count = 100;
    const size_t cell_y_count = 100;
    const double cell_width   = static_cast<double>(width-1) / cell_x_count;
    const double cell_height  = static_cast<double>(height-1) / cell_y_count;

    size_t tex_width  = texture.get_width();
    size_t tex_height = texture.get_height();

    const double cell_tex_width   = static_cast<double>(tex_width-1) / cell_x_count;
    const double cell_tex_height  = static_cast<double>(tex_height-1) / cell_y_count;

    // generate vertexes for our cell mesh
    for (size_t j = 0; j < cell_y_count; ++j)
    {
        for (size_t i = 0; i < cell_x_count; ++i)
        {
            double x = (i * cell_width);
            double y = (j * cell_height);

            double xt = floor(i * cell_tex_width);
            //if (xt>746) {xt=746;}
            double yt = floor(j * cell_tex_height);
            //if (yt>1327) {yt=1327;}

             color out_t = texture.get_pixel(xt, yt);

            triangle_v.push_back({ x,
                                   y,
                                   double(out_t.r),
                                   double(out_t.g),
                                   double(out_t.b),
                                   (xt/tex_width),
                                   (yt/tex_height)
                                   });
            //triangle_v.push_back({ x, y});
        }
    }

    std::vector<uint16_t> indexes_v{};

    // generate indexes for our cell mesh
    for (size_t j = 0; j < cell_y_count - 1; ++j)
    {
        for (size_t i = 0; i < cell_x_count - 1; ++i)
        {
            uint16_t v0 = j * cell_x_count + i;
            uint16_t v1 = v0 + 1;
            uint16_t v2 = v0 + cell_x_count;
            uint16_t v3 = v2 + 1;

            // add two triangles
            //  v0-----v1
            //  |     /|
            //  |    / |
            //  |   /  |
            //  |  /   |
            //  | /    |
            //  v2-----v3


                indexes_v.insert(end(indexes_v), { v0, v1, v2 });
                indexes_v.insert(end(indexes_v), { v2, v1, v3 });
        }
    }

    void*     pixels = image.get_pixels().data();
    const int depth  = sizeof(color) * 8;
    const int pitch  = width * sizeof(color);
    const uint32_t rmask  = 0x000000ff;
    const uint32_t gmask  = 0x0000ff00;
    const uint32_t bmask  = 0x00ff0000;
    const uint32_t amask  = 0;

    interpolated_render.set_gfx_program(*current_program);

    double mouse_x{ 100 };
    double mouse_y{ 100 };
    double radius{ 40.0 }; // 20 pixels radius

    bool continue_loop = true;

    while (continue_loop)
    {
        SDL_Event e;
        while (SDL_PollEvent(&e))
        {
            if (e.type == SDL_QUIT)
            {
                continue_loop = false;
                break;
            }
            else if (e.type == SDL_KEYUP)
            {
                current_program_index =
                    (current_program_index + 1) % programs.size();
                current_program = programs.at(current_program_index);

                interpolated_render.set_gfx_program(*current_program);
            }
            else if (e.type == SDL_MOUSEMOTION)
            {
                mouse_x = e.motion.x;
                mouse_y = e.motion.y;
            }
            else if (e.type == SDL_MOUSEWHEEL)
            {
                radius += e.wheel.y;
            }
        }

        interpolated_render.clear(black);
        double time_from_start = SDL_GetTicks() / 1000.0;
        current_program->set_uniforms(
            uniforms{ 0, 0, 0, mouse_x, mouse_y, radius, 0, time_from_start, &texture});

        interpolated_render.draw_triangles(triangle_v, indexes_v);

        SDL_Surface* bitmapSurface = SDL_CreateRGBSurfaceFrom(
            pixels, width, height, depth, pitch, rmask, gmask, bmask, amask);
        if (bitmapSurface == nullptr)
        {
            cerr << SDL_GetError() << endl;
            return EXIT_FAILURE;
        }
        SDL_Texture* bitmapTex =
            SDL_CreateTextureFromSurface(renderer, bitmapSurface);
        if (bitmapTex == nullptr)
        {
            cerr << SDL_GetError() << endl;
            return EXIT_FAILURE;
        }
        SDL_FreeSurface(bitmapSurface);

        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, bitmapTex, nullptr, nullptr);
        SDL_RenderPresent(renderer);

        SDL_DestroyTexture(bitmapTex);
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();

    return EXIT_SUCCESS;
}
